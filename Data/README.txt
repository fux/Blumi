File Liste_von_Gehoelzen_der_Schweiz.ods 
----------------------------------------
- Copied (Copy&Paste) from https://de.wikipedia.org/w/index.php?title=Liste_von_Geh%C3%B6lzen_in_der_Schweiz&oldid=148152020 (20161504) into LibreOffice Calc

File Liste_von_Gehoelzen_der_Schweiz.csv
----------------------------------------
- File saved as csv (Comma Separated Values) from .ods file above (in LibreOffice Calc) with ";" as separator


Datei dbpedia_Alpenflora.csv
----------------------------

Erstellt durch SPARQL-Query auf http://de.dbpedia.org/sparql mittels CSV-Ausgabe:

select ?r ?label ?abstract ?bild ?bildbeschreibung ?scientificName ?taxonName ?taxonWissname ?taxonRang ?taxon2Name ?taxon2Wissname ?taxon2Rang ?taxon3Name ?taxon3Wissname ?taxon3Rang ?taxon4Name ?taxon4Wissname ?taxon4Rang ?taxon5Name ?taxon5Wissname ?taxon5Rang ?taxon6Name ?taxon6Wissname ?taxon6Rang ?wikiPage { ?r <http://purl.org/dc/terms/subject> <http://de.dbpedia.org/resource/Kategorie:Alpenflora> . ?r rdfs:label ?label . ?r <http://dbpedia.org/ontology/abstract> ?abstract . OPTIONAL { ?r <http://de.dbpedia.org/property/bild> ?bild . ?r <http://de.dbpedia.org/property/bildbeschreibung> ?bildbeschreibung } . OPTIONAL { ?r <http://de.dbpedia.org/property/scientificName> ?scientificName } . OPTIONAL { ?r <http://de.dbpedia.org/property/taxonName> ?taxonName . ?r <http://de.dbpedia.org/property/taxonWissname> ?taxonWissname . ?r <http://de.dbpedia.org/property/taxonRang> ?taxonRang } . OPTIONAL { ?r <http://de.dbpedia.org/property/taxon2Name> ?taxon2Name . ?r <http://de.dbpedia.org/property/taxon2Wissname> ?taxon2Wissname . ?r <http://de.dbpedia.org/property/taxon2Rang> ?taxon2Rang } . OPTIONAL { ?r <http://de.dbpedia.org/property/taxon3Name> ?taxon3Name . ?r <http://de.dbpedia.org/property/taxon3Wissname> ?taxon3Wissname . ?r <http://de.dbpedia.org/property/taxon3Rang> ?taxon3Rang } . OPTIONAL { ?r <http://de.dbpedia.org/property/taxon4Name> ?taxon4Name . ?r <http://de.dbpedia.org/property/taxon4Wissname> ?taxon4Wissname . ?r <http://de.dbpedia.org/property/taxon4Rang> ?taxon4Rang } . OPTIONAL { ?r <http://de.dbpedia.org/property/taxon5Name> ?taxon5Name . ?r <http://de.dbpedia.org/property/taxon5Wissname> ?taxon5Wissname . ?r <http://de.dbpedia.org/property/taxon5Rang> ?taxon5Rang } . OPTIONAL { ?r <http://de.dbpedia.org/property/taxon6Name> ?taxon6Name . ?r <http://de.dbpedia.org/property/taxon6Wissname> ?taxon6Wissname . ?r <http://de.dbpedia.org/property/taxon6Rang> ?taxon6Rang } . ?r foaf:isPrimaryTopicOf ?wikiPage }

