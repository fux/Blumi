#!/bin/bash

PICFILE=
LOGFILE="../pics2.log"

counter=0

while read line
do
#     PICFILE="`echo $line | cut -d, -f1 | cut -d\| -f1 | sed 's/\"//g'`";
    PICFILE="`echo $line | cut -d'\"' -f2 | cut -d\| -f1`";
    echo $PICFILE
 
   # To see if there are multiple picture URLs 
   PICFILES="`echo $line | cut -d'\"' -f2 | grep \|`";
    if [ "$PICFILES" ] ; then
        echo "Found: $PICFILES" >> $LOGFILE
    fi
        
    
    ((counter++))
    
    echo "URL $counter: $PICFILE";
    
    if [ -z "$PICFILE" ] ; then
        continue
    fi
    
    
    
    wget $PICFILE

    if [ $? -eq 0 ] ; then
        echo "`date` - Successfull download of $PICFILE" >> $LOGFILE
    else
        echo "`date` - Download failed for $PICFILE" >> $LOGFILE
    fi
    
#     echo "$BASEURL/$PICFILE"
#     convert "$PICFILE" -resize 300 "$PICFILE-thumbnail.jpg"

#     echo "###################################"
#     echo "# $BASEURL/$PICFILE"
#     echo "###################################"
#     echo ""
#     echo ""
#     echo ""
#     echo ""
#     echo ""
#     echo ""
#     echo ""
#     echo ""
    

    sleep 2
done < $1
