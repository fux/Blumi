#!/bin/bash

FIRSTLINE=1

for i in `cat $1 | cut -d"#" -f25`
do
    if [ $FIRSTLINE -eq 1 ] ; then
        FIRSTLINE=0
        continue;
    fi
    
    konqueror $i
done
