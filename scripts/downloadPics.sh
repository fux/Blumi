#!/bin/bash

PICFILE=
BASEURL="http://commons.wikimedia.org/wiki/Special:FilePath"

while read line
do
    PICFILE="`echo $line | cut -d# -f4`"
    
    if [ -z "$PICFILE" ] ; then
        continue
    fi
    
#     wget "$BASEURL/$PICFILE"
#     echo "$BASEURL/$PICFILE"
    convert "$PICFILE" -resize 300 "$PICFILE-thumbnail.jpg"

#     echo "###################################"
#     echo "# $BASEURL/$PICFILE"
#     echo "###################################"
#     echo ""
#     echo ""
#     echo ""
#     echo ""
#     echo ""
#     echo ""
#     echo ""
#     echo ""
    

    sleep 2
done < $1
