#!/bin/bash

tname=
sname=
ssname=
ITSSWISS=0

while read line
do
    tname="`echo $line | cut -d# -f2`"
    sname="`echo $line | cut -d# -f4`"
    
    # Check if it's Swiss wood
    while read sline
    do
        ssname="`echo $sline | cut -d\";\" -f2`"
        if [ "$ssname" = "$sname" ] ; then
            echo tname: $tname - SWISSSSSSSSSSSSSSS
            ITSSWISS=1
            break;
        fi
    done < $2
    
#     if [ $ITSSWISS -eq 0 ] ; then
#         echo tname: $tname
#     fi
    
    ITSSWISS=0
done < $1