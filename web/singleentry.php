<?php
    /**
    * @copyright Copyright 2018 Mario Fux (mario@project-flauna.org)
    * @license https://www.gnu.org/licenses/gpl.txt GNU GPL
    *
    * This file is part of Project Flauna.
    * 
    * Projekt Flauna is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
    *
    * Project Flauna is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with Project Flauna. If not, see <http://www.gnu.org/licenses/>.
    */

    include_once("templates/header.php.inc");
    include_once("templates/navigation.php.inc");
    
    $fid = $_GET['fid'];

    echo '<div class="container">
            <div class="row">
              <div class="col-sm-6">';

                // Create back link
                $backlinkParameters="";
                if (isset($_SESSION['currentQuery']) && isset($_SESSION['currentPage'])) {
                    $backlinkParameters = "?page=". $_SESSION['currentPage'] . "&name=" . $_SESSION['currentQuery'];
                } elseif (isset($_SESSION['currentQuery'])) {
                    $backlinkParameters = "?name=" . $_SESSION['currentQuery'];
                } elseif (isset($_SESSION['currentPage'])) {
                    $backlinkParameters = "?page=" . $_SESSION['currentPage'];
                }
            
                echo "<p><a href=\"display-test.php" . $backlinkParameters . "\">" . _("Back to overview") . "</a></p>";

    echo     '</div>
            <div class="col-sm-6">
                <p><a href="singleentryedit.php?fid=' . $fid . '">' . _('Edit Mode On') . '</a>';
                // TODO: Translation mode/page for data translation (in contrast to UI translation via gettext/po)
//                 echo '/ <a href="">' . _('Translation Mode On') . '</a></p>';
            echo '<p></div>
        </div>';

    $statement = $pdo->prepare("SELECT * FROM $table WHERE id = ?");
                
    $statement->execute(array($fid));
    
    $entry = $statement->fetch();
    
    $species = $entry['species'];
    $genus = $entry['genera'];
    $genusArray = explode('/', $genus);
    $genus = end($genusArray);
    $family = $entry['families'];
    $familyArray = explode('/', $family);
    $family = end($familyArray);
    $order = $entry['orders'];
    $orderArray = explode('/', $order);
    $order = end($orderArray);
    $class = $entry['classes'];
    $classArray = explode('/', $class);
    $class = end($classArray);
    $phylum = $entry['phylums'];
    $phylumArray = explode('/', $phylum);
    $phylum = end($phylumArray);
    $kingdom = $entry['kingdoms'];
    $kingdomArray = explode('/', $kingdom);
    $kingdom = end($kingdomArray);
    
    $urlArray = explode(DIRECTORY_SEPARATOR, $entry['pics']);
    $picFileName = end($urlArray);
    $picFileName = "pics" . DIRECTORY_SEPARATOR . $table . DIRECTORY_SEPARATOR . $picFileName;
    
    echo "<div class=\"row\">";
    echo "<div class=\"col-sm-6\">";
    echo "<h3>" . $entry['label'] . "</h3>";
    echo "<a href=\"" . $picFileName . "\"><img title=\"" ._("Click to see in full size") . "\" class=\"img-thumbnail\" src=\"" . $picFileName . "\" alt=\"" . $entry['label'] . "\"></a>";
    echo "<p>" . $entry['caption'] . "</p>";
    
    
//             echo "<h3>Verbreitung</h3>
//                 <div id='mapid' style='width: 1000px; height: 600px;'></div>
//                 <script>
// 
//             //     var mymap = L.map('mapid').setView([47.02685153531442, 7.738140821456909], 16b);
//                 var mymap = L.map('mapid').setView([47.02685153531442, 7.738140821456909], 16);
// 
//                 L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
//                     maxZoom: 18,
//                     attribution: 'Map data &copy; <a href='http://openstreetmap.org'>OpenStreetMap</a> contributors, ' +
//                         '<a href='http://creativecommons.org/licenses/by-sa/2.0/'>CC-BY-SA</a>, ' +
//                         'Imagery © <a href='http://mapbox.com'>Mapbox</a>',
//                     id: 'mapbox.streets'
//                 }).addTo(mymap);
// 
//                 L.marker([47.02685153531442, 7.738140821456909], {draggable: 'true' }).addTo(mymap)
//                     .bindPopup('<b>Mein Wohnort</b><br />I am a popup.').openPopup();
// 
//                     
//                 L.circle([47.027, 7.74], 50, {
//                     color: 'blue',
//                     fillColor: '#ACD',
//                     fillOpacity: 0.5
//                 }).addTo(mymap).bindPopup('I am a circle.');
//                 </script>";
    
    echo '<h3>Verbreitung</h3>';
    
    echo '<div id="mapid" style="width: 100%; height: 600px;"></div>
            <script>

                var mymap = L.map("mapid").setView([47.02685153531442, 7.738140821456909], 18);

                L.tileLayer(\'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw\', {
                    minZoom: 0,
                    maxZoom: 20,
                    attribution: \'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, \' +
                        \'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, \' +
                        \'Imagery © <a href="http://mapbox.com">Mapbox</a>\',
                    id: \'mapbox.streets\'
                }).addTo(mymap);

//                 L.marker([47.02685153531442, 7.738140821456909], {draggable: \'true\' }).addTo(mymap)
//                     .bindPopup("<b>Mein <a href=\"http://www.sumiswald.ch\">Wohnort</a></b><br />I am a popup with a dot <img src=\"dot.png\">").openPopup();


                L.circle([47.02696, 7.738009], 5, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=208\">Stieleiche (Quercus robur)</a>");
                
                L.circle([47.027175, 7.738319], 1, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=208\">Stieleiche (Quercus robur)</a>");
                
                L.circle([47.027338, 7.738393], 5, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=208\">Stieleiche (Quercus robur)</a>");
                
                
                L.circle([47.026821, 7.738299], 2, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=11\">Hasel (Corylus)</a>");
                
                L.circle([47.026923, 7.738325], 2, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=11\">Hasel (Corylus)</a>");
                
                L.circle([47.027012, 7.738358], 2, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=11\">Hasel (Corylus)</a>");
                
                L.circle([47.027133, 7.738288], 2, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=11\">Hasel (Corylus)</a>");
                
                L.circle([47.027194, 7.738342], 2, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=11\">Hasel (Corylus)</a>");
                

                L.circle([47.027194, 7.738342], 1, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=743\">Spitzahorn (Acer platanoides)</a>");
                
                L.circle([47.027194, 7.738342], 1, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=743\">Spitzahorn (Acer platanoides)</a>");
                

                L.circle([47.026709, 7.738035], 2, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=488\">Hänge-Birke (Betula pendula)</a>");
                
                L.circle([47.027022, 7.738361], 2, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=488\">Hänge-Birke (Betula pendula)</a>");
                
                L.circle([47.02715, 7.738323], 2, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=488\">Hänge-Birke (Betula pendula)</a>");
                
                L.circle([47.027214, 7.738322], 2, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=488\">Hänge-Birke (Betula pendula)</a>");
                
                
                L.circle([47.026694, 7.738137], 0.5, {
                    color: \'blue\',
                    fillcolor: \'blue\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=494\">Rasen-Vergissmeinnicht (Myosotis laxa)</a>");
                
                
                L.circle([47.026926, 7.738264], 0.5, {
                    color: \'blue\',
                    fillcolor: \'blue\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=125\">Gefleckter Aronstab (Arum maculatum)</a>");


//                 for (i = 0; i < 1000; i++) {
//                     var lat = 47.023 + Math.random()/100;
//                     var long = 7.735 + Math.random()/100;
//                     var color = Math.floor(0x00 + Math.random() * 10000);
//                     
//                     L.circle([lat, long], 2, {
//                         color: "#" + color,
//                         fillColor: "#" + color,
//                         fillOpacity: 0.5
//                     }).addTo(mymap).bindPopup("I am circle #" + i + " with color #" + color.toString(16) + ".");
//                 
//                     
//                     // document.write("Coordinates: " + lat + " - " + long + "<br />");
//                     // document.write("Color: #" + color.toString(16) + "<br />");
//                 }

//                 var popup = L.popup();
// 
//                 function onMapClick(e) {
//                     popup
//                         .setLatLng(e.latlng)
//                         .setContent("You clicked the map at " + e.latlng.toString())
//                         .openOn(mymap);
//                 }

                mymap.on(\'click\', onMapClick);

            </script>';
    
    
//             echo "<h3>Verbreitung</h3>";
//             echo '<iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://www.openstreetmap.org/export/embed.html?bbox=7.735496163368226%2C47.02582034011138%2C7.740780115127564%2C47.02788271059683&amp;layer=mapnik&amp;marker=47.02685153531442%2C7.738140821456909" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=47.02685&amp;mlon=7.73814#map=18/47.02685/7.73814">Größere Karte anzeigen</a></small>';
    echo "</div>";

    echo "<div class=\"col-sm-6\">";
    echo "<h3>&nbsp;</h3>";
    echo "<p><b>" . _('Comment') . ":</b> " . $entry['comment'] . "</p>";
    echo "<p><a href=\"" . $entry['wikiPage'] . "\">" . _('Source Wikipedia page') . "</a></p>";
    echo "<p><b>" . _('Species') . ":</b> " . $species . "</a></p>";
    echo "<p><b>" . _('Genus') . ":</b> <a href=\"display-test.php?genus=$genus\">" . $genus . "</a></p>";
    echo "<p><b>" . _('Family') . ":</b> <a href=\"display-test.php?family=$family\">" . $family . "</a></p>";
    echo "<p><b>" . _('Order') . ":</b> <a href=\"display-test.php?order=$order\">" . $order . "</a></p>";
    echo "<p><b>" . _('Class') . ":</b> <a href=\"display-test.php?class=$class\">" . $class . "</a></p>";
    echo "<p><b>" . _('Phylum') . ":</b> <a href=\"display-test.php?phylum=$phylum\">" . $phylum . "</a></p>";
    echo "<p><b>" . _('Kingdom') . ":</b> <a href=\"display-test.php?kingdom=$kingdom\">" . $kingdom . "</a></p>";
    echo "<p><b>" . _('Abstract') . ":</b> " . $entry['abstract'] . "</p>";
    echo "<p><b>" . _('Same as') . ":</b>";
    $sameAsArray = explode('|', $entry['sameAss']);
    echo "<ul>";
    foreach ($sameAsArray as $sameAs) {
        echo "<li><a href=\"$sameAs\">$sameAs</a></li>";
    }
    echo "</ul></p>";
    
    echo "<p><b>" . _('Subjects/categories') . ":</b> ";
    $subjectArray = explode('|', $entry['subjects']);
    echo "<ul>";
    foreach ($subjectArray as $subject) {
        echo "<li><a href=\"$subject\">$subject</a></li>";
    }
    echo "</ul></p>";
    
    
    
    if (isset($userid)) {
        echo "<p>Hello user $userid. You might rate this entry.</p>";
    }
    
    echo "</div>";

    
    // DEBUG: Get all data and show (in two versions aka loops ;-)
//             foreach ($entry as $key => $value) {
//                 echo "<p>$key - $value</p>";
//             }
//             for ($i = 0; $i < count($entry)/2; $i++) {
//                 echo "<p>" . $entry[$i] . "</p>";
//             }


    
    echo "</div></div>";
    include_once("templates/footer.php.inc");
?>


