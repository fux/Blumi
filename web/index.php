<?php
    /**
    * @copyright Copyright 2018 Mario Fux (mario@project-flauna.org)
    * @license https://www.gnu.org/licenses/gpl.txt GNU GPL
    *
    * This file is part of Project Flauna.
    * 
    * Projekt Flauna is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
    *
    * Project Flauna is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with Project Flauna. If not, see <http://www.gnu.org/licenses/>.
    */

    // TODO: Temporary translations for the menu
    $lifedata = _("Life data");
    $getinvolved = _("Get involved");
    $aboutdata = _("About data");
    
    $a = $lifedata . $getinvolved . $aboutdata;
    
    include("templates/header.php.inc");
    include("templates/navigation.php.inc");
    echo '<div class="container">
          <div class="row">
            <div class="col-sm-12"><h2>' . _("Map the life") . '</h2>
            <p>' . _('It\'s all about <a href="aboutdata.php">open and free data</a> about our flora and fauna, where to find and determine them and how to collect more data.') . '</p>
            </div>
          </div>';
    echo '<div class="row">
            <div class="col-sm-5">
                <h3>' . _("Distribution") . '</h3>';
     echo '<div id="mapid" style="width: 100%; height: 600px;"></div>
            <script>

                var mymap = L.map("mapid").setView([47.02685153531442, 7.738140821456909], 18);

                L.tileLayer(\'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw\', {
                    minZoom: 0,
                    maxZoom: 20,
                    attribution: \'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, \' +
                        \'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, \' +
                        \'Imagery © <a href="http://mapbox.com">Mapbox</a>\',
                    id: \'mapbox.streets\'
                }).addTo(mymap);

//                 L.marker([47.02685153531442, 7.738140821456909], {draggable: \'true\' }).addTo(mymap)
//                     .bindPopup("<b>Mein <a href=\"http://www.sumiswald.ch\">Wohnort</a></b><br />I am a popup with a dot <img src=\"dot.png\">").openPopup();


                L.circle([47.02696, 7.738009], 5, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=208\">Stieleiche (Quercus robur)</a>");
                
                L.circle([47.027175, 7.738319], 1, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=208\">Stieleiche (Quercus robur)</a>");
                
                L.circle([47.027338, 7.738393], 5, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=208\">Stieleiche (Quercus robur)</a>");
                
                
                L.circle([47.026821, 7.738299], 2, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=11\">Hasel (Corylus)</a>");
                
                L.circle([47.026923, 7.738325], 2, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=11\">Hasel (Corylus)</a>");
                
                L.circle([47.027012, 7.738358], 2, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=11\">Hasel (Corylus)</a>");
                
                L.circle([47.027133, 7.738288], 2, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=11\">Hasel (Corylus)</a>");
                
                L.circle([47.027194, 7.738342], 2, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=11\">Hasel (Corylus)</a>");
                

                L.circle([47.027194, 7.738342], 1, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=743\">Spitzahorn (Acer platanoides)</a>");
                
                L.circle([47.027194, 7.738342], 1, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=743\">Spitzahorn (Acer platanoides)</a>");
                

                L.circle([47.026709, 7.738035], 2, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=488\">Hänge-Birke (Betula pendula)</a>");
                
                L.circle([47.027022, 7.738361], 2, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=488\">Hänge-Birke (Betula pendula)</a>");
                
                L.circle([47.02715, 7.738323], 2, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=488\">Hänge-Birke (Betula pendula)</a>");
                
                L.circle([47.027214, 7.738322], 2, {
                    color: \'brown\',
                    fillcolor: \'brown\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=488\">Hänge-Birke (Betula pendula)</a>");
                
                
                L.circle([47.026694, 7.738137], 0.5, {
                    color: \'blue\',
                    fillcolor: \'blue\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=494\">Rasen-Vergissmeinnicht (Myosotis laxa)</a>");
                
                
                L.circle([47.026926, 7.738264], 0.5, {
                    color: \'blue\',
                    fillcolor: \'blue\',
                    fillOpacity: 0.5
                }).addTo(mymap).bindPopup("<a href=\"http://www.project-flauna.org/bfo/singleentry.php?fid=125\">Gefleckter Aronstab (Arum maculatum)</a>");


//                 for (i = 0; i < 1000; i++) {
//                     var lat = 47.023 + Math.random()/100;
//                     var long = 7.735 + Math.random()/100;
//                     var color = Math.floor(0x00 + Math.random() * 10000);
//                     
//                     L.circle([lat, long], 2, {
//                         color: "#" + color,
//                         fillColor: "#" + color,
//                         fillOpacity: 0.5
//                     }).addTo(mymap).bindPopup("I am circle #" + i + " with color #" + color.toString(16) + ".");
//                 
//                     
//                     // document.write("Coordinates: " + lat + " - " + long + "<br />");
//                     // document.write("Color: #" + color.toString(16) + "<br />");
//                 }

//                 var popup = L.popup();
// 
//                 function onMapClick(e) {
//                     popup
//                         .setLatLng(e.latlng)
//                         .setContent("You clicked the map at " + e.latlng.toString())
//                         .openOn(mymap);
//                 }

                mymap.on(\'click\', onMapClick);

            </script>
            
                <h3>' . _("News") . '</h3>
                    
                    <h4>' . _('First alpha version of Project Flauna released') . '</h4>
                    <p><i>' . _('Saturday, April 28, 2018') . '</i></p>
                    <p>' . _('This afternoon at the <a href="https://openeducationday.ch/">Open Education Day</a> the first alpha version of Project Flauna was set live.') . '</p>
                    <p>' . _('After years of brainstorming and noting down ideas the first version of Project Flauna is finally online.') .
                    _('Project Flauna is a new open and free data platform that strives to collect biological and geographical information about life on this planet.') . '</p>
                    <p>' . _('Over the coming weeks, months and years we will release more features on this webpage and together with you it shall become the best place to collect data about and determine the fauna and flora of our home planet.') . '</p>
                    <p>' . _('Please enjoy our data and it would be great if you would start <a href="registration.php">contributing</a> too.') . '</p>
                    <p>' . _('For the case you want to stay informed about the future development of Project Flauna please subscribe to our low traffic <a href="http://lists.project-flauna.org/mailman/listinfo/newsletter-de">newsletter</a>.') . '</p>
                    
                    <h4>' . _('Nicer pictures and last data scheme change') . '</h4>
                    <p><i>' . _('XYday, May xy, 2018') . '</i></p>
                    <p>' . _('All the current pictures got squared and thus a better layout is possible now.') . '</p>
                    <p>' . _('Furthermore we did a (hopefully) last big change of the data layout.') .
                    _('So from now on (and to for the next billion years) all species have their own, single and unique FID (Flauna ID).') . '</p>
                    
            </div>
            <div class="col-sm-7">';
             
            // TODO: Add random (DONE) but valid FID here ;-)
            $fid = rand(1,500);
                    
            $statement = $pdo->prepare("SELECT * FROM $table WHERE id = ?");
                      
            $statement->execute(array($fid));
            
            $entry = $statement->fetch();
            
            $species = $entry['species'];
            $genus = $entry['genera'];
            $genusArray = explode('/', $genus);
            $genus = end($genusArray);
            $family = $entry['families'];
            $familyArray = explode('/', $family);
            $family = end($familyArray);
            $order = $entry['orders'];
            $orderArray = explode('/', $order);
            $order = end($orderArray);
            $class = $entry['classes'];
            $classArray = explode('/', $class);
            $class = end($classArray);
            $phylum = $entry['phylums'];
            $phylumArray = explode('/', $phylum);
            $phylum = end($phylumArray);
            $kingdom = $entry['kingdoms'];
            $kingdomArray = explode('/', $kingdom);
            $kingdom = end($kingdomArray);
            
            $urlArray = explode(DIRECTORY_SEPARATOR, $entry['pics']);
            $picFileName = end($urlArray);
            $picFileName = "pics" . DIRECTORY_SEPARATOR . $table . DIRECTORY_SEPARATOR . $picFileName;
            $templatePic = "white-template.png";
            
            echo "<h3>" . $entry['label'] . "</h3>"; //TODO: Add link to new random being
            if(file_exists($picFileName)) {
                echo "<a href=\"" . $picFileName . "\"><img title=\"" . _("Click to see in full size") . "\" class=\"img-thumbnail\" src=\"" . $picFileName;
            } else {
                echo "<a href=\"" . $templatePic . "\"><img title=\"" . _("Click to see in full size") . "\" class=\"img-thumbnail\" src=\"" . $templatePic;
            }
            echo "\" alt=\"" . $entry['label'] . "\"></a>";
            echo "<p><i>" . $entry['caption'] . ' (Picture from <a href="https://commons.wikimedia.org/wiki/Main_Page">WikiCommons</a>)</i></p>';
                                   
            echo "<ul class=\"breadcrumb\">
                  <li><a href=\"display-test.php?kingdom=$kingdom\">$kingdom</a></li>
                  <li><a href=\"display-test.php?phylum=$phylum\">$phylum</a></li>
                  <li><a href=\"display-test.php?class=$class\">$class</a></li>
                  <li><a href=\"display-test.php?order=$order\">$order</a></li>
                  <li><a href=\"display-test.php?family=$family\">$family</a></li>
                  <li><a href=\"display-test.php?genus=$genus\">$genus</a></li>
                  <li><a href=\"singleentry.php?fid=$fid\">$species</a></li></ul>";
            
            echo "<p><b>" . _('Comment') . ":</b> " . $entry['comment'] . "</p>";
            echo "<p><a href=\"" . $entry['wikiPage'] . "\">" . _('Source Wikipedia page') . "</a></p>";

            echo "<p><b>" . _('Same as') . ":</b>";
            $sameAsArray = explode('|', $entry['sameAss']);
            echo "<ul>";
            foreach ($sameAsArray as $sameAs) {
                echo "<li><a href=\"$sameAs\">$sameAs</a></li>";
            }
            echo "</ul></p>";
            
            echo "<p><b>" . _('Subjects/categories') . ":</b> ";
            $subjectArray = explode('|', $entry['subjects']);
            echo "<ul>";
            foreach ($subjectArray as $subject) {
                echo "<li><a href=\"$subject\">$subject</a></li>";
            }
            echo "</ul></p>";
            
            
            
            if (isset($userid)) {
                echo "<p>Hello user $userid. You might rate this entry.</p>";
            }
            
            echo "</div>";
             
       echo '</div></div></div>';
          
//     echo '<div class="row">
//               <div class="col-sm-6">
//                   <h3>' . _("Code of Conduct - This is the way we want to work together:") . '</h3>
//                   <ul>
//                     <li>' . _('We always assume good intention in the communication and interaction with others.') . '</li>
//                     <li>' . _('We include everybody who wants to work with us.') . '</li>
//               </div>
//           </div>';

    
    include("templates/footer.php.inc");
?>

