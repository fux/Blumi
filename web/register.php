<!DOCTYPE html> 
<html> 
    <head>
        <title>Registration</title> 
    </head> 
<body>
 
<?php
$showFormular = true;
 
if(isset($_GET['register'])) {
 $error = false;
 $email = $_POST['email'];
 $password = $_POST['password'];
 $password2 = $_POST['password2'];
  
 if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
 echo 'Please enter a valid email address<br>';
 $error = true;
 } 
 if(strlen($password) == 0) {
 echo 'Please enter a password<br>';
 $error = true;
 }
 if($password != $password2) {
 echo 'The passwords need to match<br>';
 $error = true;
 }
 
 // Check if email address is not already in use
 if(!$error) { 
 $statement = $pdo->prepare("SELECT * FROM users WHERE email = :email");
 $result = $statement->execute(array('email' => $email));
 $user = $statement->fetch();
 
 if($user !== false) {
 echo 'This email address is already in use.<br>';
 $error = true;
 } 
 }
 
 // No errors we can register the user
 if(!$error) { 
 $password_hash = password_hash($password, PASSWORD_DEFAULT);
 
 $statement = $pdo->prepare("INSERT INTO users (email, password) VALUES (:email, :password)");
 $result = $statement->execute(array('email' => $email, 'password' => $password_hash));
 
 if($result) { 
 echo 'You successfully registered. <a href="display-test.php">Back to login</a>';
 $showFormular = false;
 } else {
 echo 'An error happened.<br>';
 }
 } 
}
 
if($showFormular) {
?>
 
<form action="?register=1" method="post">
Email address:<br>
<input type="email" size="40" maxlength="250" name="email"><br><br>
 
Your password:<br>
<input type="password" size="40"  maxlength="250" name="password"><br>
 
Repeat password:<br>
<input type="password" size="40" maxlength="250" name="password2"><br><br>
 
<input type="submit" value="Register">
</form>
 
<?php
} //End of if($showFormular)
?>
 
</body>
</html>
