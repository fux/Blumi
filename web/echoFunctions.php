<?php
    function echoFormInput($inputName, $label, $value = "") {
        echo '<div class="form-group">';
        echo '    <label class="control-label col-sm-2" for="' . $inputName . '">' . $label . ': </label>';
        echo '    <div class="col-sm-10">';
        echo '        <input class="form-control" type="text" name="' . $inputName . '" id="' . $inputName . '" value="' . $value . '"/>';
        echo '    </div>';
        echo '</div>';
    }
?>
