<?php
    /**
    * @copyright Copyright 2018 Mario Fux (mario@project-flauna.org)
    * @license https://www.gnu.org/licenses/gpl.txt GNU GPL
    *
    * This file is part of Project Flauna.
    * 
    * Projekt Flauna is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
    *
    * Project Flauna is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with Project Flauna. If not, see <http://www.gnu.org/licenses/>.
    */

    include("templates/header.php.inc");
    include("templates/navigation.php.inc");
    echo '<div class="container">
          <div class="row">
              <div class="col-sm-12"><h2>' . _("About the data") . '</h2></div>
          </div>';
    echo '<div class="row">
              <div class="col-sm-12">
                  <h3>' . _("Where is the data for this webpage coming from?") . '</h3>
                  <p>' . _('As with most of the Open Data and Free Software projects this projects is standing on the shoulders of giants:') . '</p>
                  <ul>
                    <li>' . _('Most of the data at the beginning of this project came through <a href="http://wiki.dbpedia.org/">DBpedia</a> and from <a href="https://www.wikipedia.org">Wikipedia</a>. See the licence information for the data items and pictures to get precise information.') . '</li>
                    <li>' . _('All the maps come from the awesome <a href="http://www.openstreetmap.org">OpenStreetMap</a> project.') . '</li>
                    <li>' . _('All the other data is being contributed by the users of this webpage and are licensed under the term of the <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.de">Creative Commons BY SA 4.0 International</a>.') . '</li>
                    <li>' . _('All new data gets trough a review process and thus for every data item three persons need to check before it gets valid data.') . '</li>
                  </ul>
                  <p>' . _('Should you find any missing or wrong licensing information please don\'t hesitate to <a href="mailto:bugs@project-flauna.org">contact us</a> so we can fix it as soon as possible.') . '</p>
              </div>
          </div>';
          
    echo '<div class="row">
              <div class="col-sm-12">
                  <h3>' . _('What software is being use for this webpage?') . '</h3>
                  <ul>
                    <li>' . _('For the webpage we use PHP, JavaScript and a MySQL database.') . '</li>
                    <li>' . _('For the scripts we use Python and Bash.') . '</li>
                    <li>' . _('For image manipulation we use the ImageMagick framework.') . '</li>
                    <li>' . _('For the maps we use OpenLayers or Leaflet.js') . '</li>
                    <li>' . _('For the application we use Qt, KDE Frameworks and Kirigami') . '</li>
                  </ul>
              </div>
          </div>';

    
    include("templates/footer.php.inc");
?>

