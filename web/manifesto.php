<?php
    /**
    * @copyright Copyright 2018 Mario Fux (mario@project-flauna.org)
    * @license https://www.gnu.org/licenses/gpl.txt GNU GPL
    *
    * This file is part of Project Flauna.
    * 
    * Projekt Flauna is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
    *
    * Project Flauna is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with Project Flauna. If not, see <http://www.gnu.org/licenses/>.
    */

    include("templates/header.php.inc");
    include("templates/navigation.php.inc");
    echo '<div class="container">
          <div class="row">
              <div class="col-sm-12"><h2>' . _("Manifesto") . '</h2></div>
          </div>';
    echo '<div class="row">
              <div class="col-sm-12">
                  <h3>' . _("These are the goals and rules of our project:") . '</h3>
                  <ul>
                    <li>' . _('Our data and community shall always be free and open to use, investigate and distribute for every purpose.') . '</li>
                    <li>' . _('Our data shall be scientifically correct - the presentation of the data shall be simple for laymen be precise as science.') . '</li>
                    <li>' . _('Our community shall be open for everybody.') . '</li>
                    <li>' . _('We strive to create the best applications and webpage to determine all animals and plants.') . '</li>
                    <li>' . _('We strive to collect accurate data about all animals and plants living and have lived on this planet and other places whereever beings using our webpage live.') . '</li>
                    <li>' . _('As every project this one has an end goal to: To map all the data of all life in our surroundings.') . '</li>
                  </ul>
              </div>
          </div>';
          
    echo '<div class="row">
              <div class="col-sm-12">
                  <h3>' . _("Code of Conduct - This is the way we want to work together:") . '</h3>
                  <ul>
                    <li>' . _('We always assume good intention in the communication and interaction with others.') . '</li>
                    <li>' . _('We include everybody who wants to work with us.') . '</li>
              </div>
          </div>';

    
    include("templates/footer.php.inc");
?>

