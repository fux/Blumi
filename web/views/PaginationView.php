<?php
    /**
    * @copyright Copyright 2018 Mario Fux (mario@project-flauna.org)
    * @license https://www.gnu.org/licenses/gpl.txt GNU GPL
    *
    * This file is part of Project Flauna.
    * 
    * Projekt Flauna is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
    *
    * Project Flauna is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with Project Flauna. If not, see <http://www.gnu.org/licenses/>.
    */


    class PaginationView {
    
        private $pageName = "display-test.php";
    
        /** 
        * Constructor
        * 
        * @param 
        */  
        public function __construct() {
        
        }
        
        /**
        * Displays the pagination including links and in correct layout
        *
        * @param Integer $currentPage       Current page
        * @param Integer $noOfPages         Number of pages
        * @param Integer $paginationSize    Number of Pages before and after current page
        * @param Integer $name              Search name
        */        
        public function display($currentPage, $noOfPages, $paginationSize, $name = "") {
                echo "<ul class=\"pagination\">";
                if (!isset($currentPage)) {
                    $currentPage = 1;
                }
                
                if ($name == "") {
                    unset($name);
                }

                // Set lower range of pagination
                if ($currentPage-$paginationSize < 1) {
                    $i = 1;
                } else {
                    $i = $currentPage - $paginationSize;
                }

                // Set upper range of pagination
                if ($currentPage+$paginationSize > $noOfPages) {
                    $end = $noOfPages;
                    $noNext = true;
                } else {
                    $end = $currentPage + $paginationSize;
                    $noNext = false;
                }

                // Create first and previous link
                if ($currentPage != 1) {
                    if (isset($name)) {
                        echo "<li><a href=\"" . $this->pageName . "?page=1&name=$name\">&lt;&lt;</a></li>";
                        echo "<li><a href=\"" . $this->pageName . "?page=" . intval($currentPage-1) . "&name=$name\">&lt;</a></li>";
                    } else {
                        echo "<li><a href=\"" . $this->pageName . "?page=1\">&lt;&lt;</a></li>";
                        echo "<li><a href=\"" . $this->pageName . "?page=" . intval($currentPage-1) . "\">&lt;</a></li>";
                    }
                }
                
                // Create regular links
                for ($i; $i <= $end; $i++) {
                    if (isset($currentPage) && $i == $currentPage) {
                        $activeAttribute="class=\"active\"";
                    } else {
                        $activeAttribute="";
                    }
                
                    if (isset($name)) {
                        echo "<li $activeAttribute><a href=\"" . $this->pageName . "?page=$i&name=$name\">$i</a></li>";
                    } else {
                        echo "<li $activeAttribute><a href=\"" . $this->pageName . "?page=$i\">$i</a></li>";
                    }
                    $activeAttribute="";
                }
                
                // Create next and end links
                if ($currentPage != $noOfPages) {
                    if (isset($name)) {
                        echo "<li><a href=\"" . $this->pageName . "?page=" . intval($currentPage+1) . "&name=$name\">&gt;</a></li>";
                        echo "<li><a href=\"" . $this->pageName . "?page=$noOfPages&name=$name\">&gt;&gt;</a></li>";
                    } else {
                        echo "<li><a href=\"" . $this->pageName . "?page=" . intval($currentPage+1) . "\">&gt;</a></li>";
                        echo "<li><a href=\"" . $this->pageName . "?page=$noOfPages\">&gt;&gt;</a></li>";
                    }
                }
                
                echo "</ul>";
        
        }
    
    }
?>
 
