<?php
    /**
    * @copyright Copyright 2018 Mario Fux (mario@project-flauna.org)
    * @license https://www.gnu.org/licenses/gpl.txt GNU GPL
    *
    * This file is part of Project Flauna.
    * 
    * Projekt Flauna is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
    *
    * Project Flauna is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with Project Flauna. If not, see <http://www.gnu.org/licenses/>.
    */

    include_once("templates/header.php.inc");
    
    echo '<div class="alert alert-info fade in alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>' . ('Info!') . '</strong> ' . _('This page is not yet fully working') . ' ' . 
                    _('We are working hard to get this feature completed for you!') .
                 '</div>';
    
    include_once("templates/navigation.php.inc");
    include_once("echoFunctions.php");
    
    $fid = $_GET['fid'];

    echo '<form class="form-horizontal" action="" method="POST">';

    echo '<div class="container">
            <div class="row">
              <div class="col-sm-6">';

                // Create back link
                $backlinkParameters="";
                if (isset($_SESSION['currentQuery']) && isset($_SESSION['currentPage'])) {
                    $backlinkParameters = "?page=". $_SESSION['currentPage'] . "&name=" . $_SESSION['currentQuery'];
                } elseif (isset($_SESSION['currentQuery'])) {
                    $backlinkParameters = "?name=" . $_SESSION['currentQuery'];
                } elseif (isset($_SESSION['currentPage'])) {
                    $backlinkParameters = "?page=" . $_SESSION['currentPage'];
                }
            
                echo "<p><a href=\"display-test.php" . $backlinkParameters . "\">" . _("Back to overview") . "</a></p>";

    echo     '</div>
            <div class="col-sm-6">
                <p><a href="singleentry.php?fid=' . $fid . '">' . _('Edit Mode Off') . '</a></p>
            </div>
        </div>';

    $statement = $pdo->prepare("SELECT * FROM $table WHERE id = ?");
                
    $statement->execute(array($fid));
    
    $entry = $statement->fetch();
    
    $species = $entry['species'];
    $genus = $entry['genera'];
    $genusArray = explode('/', $genus);
    $genus = end($genusArray);
    $family = $entry['families'];
    $familyArray = explode('/', $family);
    $family = end($familyArray);
    $order = $entry['orders'];
    $orderArray = explode('/', $order);
    $order = end($orderArray);
    $class = $entry['classes'];
    $classArray = explode('/', $class);
    $class = end($classArray);
    $phylum = $entry['phylums'];
    $phylumArray = explode('/', $phylum);
    $phylum = end($phylumArray);
    $kingdom = $entry['kingdoms'];
    $kingdomArray = explode('/', $kingdom);
    $kingdom = end($kingdomArray);
    
    $urlArray = explode(DIRECTORY_SEPARATOR, $entry['pics']);
    $picFileName = end($urlArray);
    $picFileName = "pics" . DIRECTORY_SEPARATOR . $table . DIRECTORY_SEPARATOR . $picFileName;
    
    echo "<div class=\"row\">";
    echo "<div class=\"col-sm-6\">";
    
    echoFormInput("label", _('Label'), $entry['label']);
//     echo "<h3>" . $entry['label'] . "</h3>";

    echo "<a href=\"" . $picFileName . "\"><img title=\"" ._("Click to see in full size") . "\" class=\"img-thumbnail\" src=\"" . $picFileName . "\" alt=\"" . $entry['label'] . "\"></a>";
    
    echoFormInput("caption", _('Caption'), $entry['caption']);
    
//     echo "<p>" . $entry['caption'] . "</p>";
    
    
//             echo "<h3>Verbreitung</h3>
//                 <div id='mapid' style='width: 1000px; height: 600px;'></div>
//                 <script>
// 
//             //     var mymap = L.map('mapid').setView([47.02685153531442, 7.738140821456909], 16b);
//                 var mymap = L.map('mapid').setView([47.02685153531442, 7.738140821456909], 16);
// 
//                 L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
//                     maxZoom: 18,
//                     attribution: 'Map data &copy; <a href='http://openstreetmap.org'>OpenStreetMap</a> contributors, ' +
//                         '<a href='http://creativecommons.org/licenses/by-sa/2.0/'>CC-BY-SA</a>, ' +
//                         'Imagery © <a href='http://mapbox.com'>Mapbox</a>',
//                     id: 'mapbox.streets'
//                 }).addTo(mymap);
// 
//                 L.marker([47.02685153531442, 7.738140821456909], {draggable: 'true' }).addTo(mymap)
//                     .bindPopup('<b>Mein Wohnort</b><br />I am a popup.').openPopup();
// 
//                     
//                 L.circle([47.027, 7.74], 50, {
//                     color: 'blue',
//                     fillColor: '#ACD',
//                     fillOpacity: 0.5
//                 }).addTo(mymap).bindPopup('I am a circle.');
//                 </script>";
    
    echo '<h3>Verbreitung</h3>
        <div id="map" class="map"></div>
        <script type="text/javascript">
            const map = new ol.Map({
                target: \'map\',
                layers: [
                    new ol.layer.Tile({
                        source: new ol.source.OSM()
                    })
                ],
                view: new ol.View({
                    center: ol.proj.fromLonLat([7.738140821456909, 47.02685153531442]),
                    zoom: 17
                })
            });
            const position = new VectorSource();
            const vector = new VectorLayer({
                source: position
            });
            map.addLayer(vector);
            
            navigator.geolocation.getCurrentPosition(function(pos) {
                const coords = ol.proj.fromLonLat([pos.coords.longitude, pos.coords.latitude]);
                map.getView().animate({center: coords, zoom: 10});
            });

            
            position.addFeature(new Feature(new Point(coords)));
            vector.setStyle(new Style({
                image: new IconStyle({
                    src: \'dot.png\'
                })
            }));
    </script>';
    
    
//             echo "<h3>Verbreitung</h3>";
//             echo '<iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://www.openstreetmap.org/export/embed.html?bbox=7.735496163368226%2C47.02582034011138%2C7.740780115127564%2C47.02788271059683&amp;layer=mapnik&amp;marker=47.02685153531442%2C7.738140821456909" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=47.02685&amp;mlon=7.73814#map=18/47.02685/7.73814">Größere Karte anzeigen</a></small>';
    echo "</div>";

    echo "<div class=\"col-sm-6\">";
    
    echoFormInput("comment", _('Comment'), $entry['comment']);
//     echo "<p><b>Comment:</b> " . $entry['comment'] . "</p>";
    
    echoFormInput("wikiPage", _('Source Wikipedia page'), $entry['wikiPage']);
//     echo "<p><a href=\"" . $entry['wikiPage'] . "\">Source Wikipedia page</a></p>";

    echoFormInput("species", _('Species'), $species);
//     echo "<p><b>Species:</b> " . $species . "</a></p>";

    echoFormInput("genus", _('Genus'), $genus);
//     echo "<p><b>Genus:</b> <a href=\"display-test.php?genus=$genus\">" . $genus . "</a></p>";

    echoFormInput("family", _('Family'), $family);
//     echo "<p><b>Family:</b> <a href=\"display-test.php?family=$family\">" . $family . "</a></p>";
    
    echoFormInput("order", _('Order'), $order);
//     echo "<p><b>Order:</b> <a href=\"display-test.php?order=$order\">" . $order . "</a></p>";
    
    echoFormInput("class", _('Class'), $class);
//     echo "<p><b>Class:</b> <a href=\"display-test.php?class=$class\">" . $class . "</a></p>";
    
    echoFormInput("phylum", _('Phylum'), $phylum);
//     echo "<p><b>Phylum:</b> <a href=\"display-test.php?phylum=$phylum\">" . $phylum . "</a></p>";
    
    echoFormInput("abstract", _('Kingdom'), $kingdom);
//     echo "<p><b>Kingdom:</b> <a href=\"display-test.php?kingdom=$kingdom\">" . $kingdom . "</a></p>";
    
    echoFormInput("species", _('Abstract'), $entry['abstract']);
//     echo "<p><b>Abstract:</b> " . $entry['abstract'] . "</p>";
    
    
    echo "<p><b>Same as:</b>";
    $sameAsArray = explode('|', $entry['sameAss']);
    echo "<ul>";
    foreach ($sameAsArray as $sameAs) {
        echo "<li><a href=\"$sameAs\">$sameAs</a></li>";
    }
    echo "</ul></p>";
    
    echo "<p><b>Subjects/categories:</b> ";
    $subjectArray = explode('|', $entry['subjects']);
    echo "<ul>";
    foreach ($subjectArray as $subject) {
        echo "<li><a href=\"$subject\">$subject</a></li>";
    }
    echo "</ul></p>";
    
    
    
    if (isset($userid)) {
        echo "<p>Hello user $userid. You might rate this entry.</p>";
    }
    
    echo "</div>";

    
    // DEBUG: Get all data and show (in two versions aka loops ;-)
//             foreach ($entry as $key => $value) {
//                 echo "<p>$key - $value</p>";
//             }
//             for ($i = 0; $i < count($entry)/2; $i++) {
//                 echo "<p>" . $entry[$i] . "</p>";
//             }


    
    echo "</div></div></form>";
    include_once("templates/footer.php.inc");
?>


