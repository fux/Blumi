<?php
    /**
    * @copyright Copyright 2018 Mario Fux (mario@project-flauna.org)
    * @license https://www.gnu.org/licenses/gpl.txt GNU GPL
    *
    * This file is part of Project Flauna.
    * 
    * Projekt Flauna is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
    *
    * Project Flauna is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with Project Flauna. If not, see <http://www.gnu.org/licenses/>.
    */

    //TODO: Fix active class <li class=active"> in navigation below

    if(isset($_GET['logout']) && $_GET['logout'] == 1) {
        echo '<div class="alert alert-success fade in alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>' . _('Success!') . '</strong> ' . _('You are logged out.') . ' ' . _('So long and thanks for all the fish.') . 
                 '</div>';
    
        session_destroy();
        unset($userid);
    }

    echo '<nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                <a class="navbar-brand" href="index.php">Project Flauna</a>
                </div>
                <ul class="nav navbar-nav">
                <li><a href="display-test.php">' . _('Life data') . '</a></li>
                <li><a href="getinvolved.php">' . _('Get involved') . '</a></li>
                <li><a href="manifesto.php">' . _('Manifesto') . '</a></li>
                <li><a href="aboutdata.php">' . _('About data') . '</a></li>';
                if(isset($userid)) {
                    echo '<li><a href="userprofile.php">' . _('User Profile') . '</a></li>';
                    echo '<li><a href="?logout=1">' . _('Logout') . '</a></li>';
                } else {
                    echo '<li><a href="login.php">'. _('Login') . '</a></li>
                          <li><a href="registration.php">' . _('Register') . '</a></li>';
                }
    echo '</ul>
                <form class="navbar-form navbar-right" action="display-test.php" method="GET">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="' . _('Search') . '" name="name">
                        <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                        </div>
                    </div>
                </form>
            </div>
        </nav>';
?>
