<?php
    /**
    * @copyright Copyright 2018 Mario Fux (mario@project-flauna.org)
    * @license https://www.gnu.org/licenses/gpl.txt GNU GPL
    *
    * This file is part of Project Flauna.
    * 
    * Projekt Flauna is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
    *
    * Project Flauna is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with Project Flauna. If not, see <http://www.gnu.org/licenses/>.
    */

    session_start();

    include_once("db/db_connection.php.inc");
    
    $language = 'de_CH.utf8';
    putenv("LANG=$language"); 

    setlocale(LC_ALL, $language);
    
    $domain = 'blumi';
    bindtextdomain($domain, "./locale"); 
    textdomain($domain);
    bind_textdomain_codeset($domain, 'UTF-8');
    
    // Letters for registration verification
    $letterA = "A";
    $letterB = "B";

    if (isset($_POST['db'])) {
        $table = $_POST['db'];
        $_SESSION['db'] = $table;
    } elseif (isset($_SESSION['db'])) {
        $table = $_SESSION['db'];
    }
    
    if(isset($_GET['login'])) {
        $email = $_POST['email'];
        $password = $_POST['password'];

        $statement = $pdo->prepare("SELECT * FROM users WHERE email = :email");
        $result = $statement->execute(array('email' => $email));
        $user = $statement->fetch();
        
        // Checking password
        if ($user !== false && password_verify($password, $user['password']) && $user['verified'] == 1) {
            $_SESSION['userid'] = $user['id'];
            echo '<div class="alert alert-success fade in alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>' , _('Success!') . '</strong> ' . _('You are now logged in.') . ' ' . _('Thanks for your contributions!') .
                 '</div>';
        } else {
            echo '<div class="alert alert-danger fade in alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>' . ('Warning!') . '</strong> ' . _('Login didn\'t work.') .
                    _('Your password might be wrong or your account is not yet verified?') .
                 '</div>';
        }

    }
    
    if (isset($_SESSION['userid'])) {
        $userid = $_SESSION['userid'];
    }
?>

<!DOCTYPE HTML>
<html lang="en">
    <head>
        <title>Project Flauna - Map the Life</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--         <link rel="stylesheet" href="https://openlayers.org/en/v4.6.4/css/ol.css" type="text/css"> -->
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"
   integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
   crossorigin=""/>
     <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"
   integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="
   crossorigin=""></script>
<!--        <style>
            .map {
            height: 400px;
            width: 100%;
            }
        </style>
        <script src="https://openlayers.org/en/v4.6.4/build/ol.js" type="text/javascript"></script>-->
<!--         <script src="../bootstrap/jquery.min.js"></script> -->
<!--         <script src="../bootstrap/bootstrap.min.js"></script> -->
        <link rel="stylesheet" href="bootstrap/bootstrap.min.css">

    </head>
    <body>
