<?php
    /**
    * @copyright Copyright 2018 Mario Fux (mario@project-flauna.org)
    * @license https://www.gnu.org/licenses/gpl.txt GNU GPL
    *
    * This file is part of Project Flauna.
    * 
    * Projekt Flauna is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
    *
    * Project Flauna is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with Project Flauna. If not, see <http://www.gnu.org/licenses/>.
    */

    include("templates/header.php.inc");
    include("templates/navigation.php.inc");
    
    $showForm = true;
    $nameEmpty = false;
    $emailEmpty = false;
    $agreementEmpty = false;
    $passwordEmpty = false;
    $formFieldMissing = false;
    $differentPasswords = false;
    
    if(isset($_POST['register'])) {
        $showForm = false;
        
        if($_POST['name'] == "") {
            $nameEmpty = true;
            $formFieldMissing = true;
            $showForm = true;
        }
        
        if($_POST['email'] == "") {
            $emailEmpty = true;
            $formFieldMissing = true;
            $showForm = true;
        }
        
        if($_POST['password'] == ""){
            $passwordEmpty = true;
            $formFieldMissing = true;
            $showForm = true;        
        }
        
        if($_POST['password'] != $_POST['password2']){
            $differentPasswords = true;
            $showForm = true;
        }
        
        if (!isset($_POST['agreeManifesto']) || !isset($_POST['agreeLicense'])) {
            $agreementEmpty = true;
            $showForm = true;
            $formFieldMissing = true;
        }
    }
    
    echo '<div class="container">
          <div class="row">
              <div class="col-sm-12"><h2>' . _("Register") . '</h2></div>
          </div>';
    echo '<div class="row">
              <div class="col-sm-12">';  
                if ($showForm) {
                    if($formFieldMissing) {
                        echo '<div class="alert alert-danger fade in alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Oiwei!</strong> ' . _('The form is missing some data.');
                            if($nameEmpty) {
                                echo "";
                            }
                            
                            if($agreementEmpty) {
                                echo _('You must agree to both the <a href="">license</a> and the <a href="manifesto.php">manifesto</a>.');
                            }
                                
                        echo '</div>';
                    }
              
                    echo '<form class="form-horizontal" action="registration.php" method="POST">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">*' . _('Email') . ':</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="email" id="email" name="email" placeHolder="' . _("Email") . '">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">*' . _('Password') . ':</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="password" id="pwd" name="password" placeHolder="' . _("Password") . '">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd2">*' . _('Password again') . ':</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="password" id="pwd2" name="password2" placeHolder="' . _("Password2") . '">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="name">*' . _('Name') . ':</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" id="name" name="name" placeHolder="' . _("Name") . '">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="lastname">' . _('Lastname') . ':</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" id="lastname" name="lastname" placeHolder="' . _("Lastname") . '">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="country">' . _('Country') . ':</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" id="country" name="country" placeHolder="' . _("Country") . '">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="phone">' . _('Phone Number') . ':</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" id="phone" name="phone" placeHolder="' . _("Phone Number") . '">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label><input type="checkbox" name="agreeManifesto">*' . _('I herewith agree to the <a href="manifesto.php">manifesto</a> of this project.') . '</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label><input type="checkbox" name="agreeLicense">*' . _('I herewith agree that all the data I contribute to this project will be licensed under the terms of the <a href="https://creativecommons.org/licenses/by-nd/4.0/deed.de">Creative Commons BY SA 4.0 International</a>.') . '</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" name="register" class="btn btn-default">' . _("Register") . '</button>
                        </div>
                    </div>
                    </form>';
                    echo '<div class="col-sm-offset-2 col-sm-10"><p><i>' . _('Entries marked with a * are mandatory.') , '</i></p></div>';
               } else {
                   // TODO: Register the user, create new User() and verify i.e. give feedback to the user about it
                    $password = $_POST['password'];
                    $email = $_POST['email'];
                    $name = $_POST['name'];
                    $lastname = $_POST['lastname'];
                    $country = $_POST['country'];
                    $phone = $_POST['phone'];
                    $password_hash = password_hash($password, PASSWORD_DEFAULT);
 
                    $statement = $pdo->prepare("INSERT INTO users (email, password, name, lastname, country, phone)
                                                VALUES (:email, :password, :name, :lastname, :country, :phone)");
                    $result = $statement->execute(array('email' => $email, 'password' => $password_hash, 'name' => $name, 'lastname' => $lastname,
                                                        'country' => $country, 'phone' => $phone));
                    $id = $pdo->lastInsertId();
                    
                    if($result) { 
                        echo _('You successfully registered.') . ' ' . _('Please verify your registration according to the email we just sent you.');
                        $showFormular = false;
                    } else {
                        echo 'An error happened.<br>';
                    }
                    
                    $subject = _("How to verify your Project Flauna account");
                    $letters = $letterA . " " . $letterB;
                    // Generate random verification code/hash
                    $verificationCode = uniqid();
                    $verificationCodeWithLetters = $verificationCode . $letterA . $letterB;

                    // INSERT into verificationTable
                    $statement = $pdo->prepare("INSERT INTO verification (userid, verificationCode) VALUES (:id, :verificationCode)");
                    $statement->execute(array('id' => $id, 'verificationCode' => $verificationCode));
                    
                    $URL = "http://www.project-flauna.org/verify.php?c=" . $verificationCode;
                    $mailBody = _("Dear") . " $name" . "\n" . "\n"
                                . _("Thanks a lot for contributing to Project Flauna.") . "\n"
                                . _("To verify your account please the upper case versions of the following letters to the URL below: ") . $letters . "\n"
                                . _("Copy and paste this link to the address line of your web browser and add the above mentioned letters.") . "\n"
                                . $URL . "\n"
                                . "\n"
                                . _("Best wishes") . "\n"
                                . _("The Project Flauna team");
                    $from = "From: Project Flauna <info@project-flauna.org>";
                                
                    // DEBUG output
//                     echo "<p>$mailBody</p>";
                    
                    mail($email, $subject, $mailBody, $from);
               }
    echo '    </div>
          </div></div>';
    
    include("templates/footer.php.inc");
?>
