<?php
    /**
    * @copyright Copyright 2018 Mario Fux (mario@project-flauna.org)
    * @license https://www.gnu.org/licenses/gpl.txt GNU GPL
    *
    * This file is part of Project Flauna.
    * 
    * Projekt Flauna is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
    *
    * Project Flauna is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with Project Flauna. If not, see <http://www.gnu.org/licenses/>.
    */

    include("templates/header.php.inc");
    include("templates/navigation.php.inc");
    
    $showForm = true;
    
    if(isset($_POST['register'])) {
        $showForm = false;
    }
    
    echo '<div class="container">
          <div class="row">
              <div class="col-sm-12"><h2>' . _("Login") . '</h2></div>
          </div>';
    echo '<div class="row">
              <div class="col-sm-12">';  
              if ($showForm) {
                  echo '<form class="form-horizontal" action="login.php?login=1" method="POST">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">*' . _('Email') . ':</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="email" name="email" id="email" placeHolder="' . _("Email") . '">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">*' . _('Password') . ':</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="password" name="password" id="pwd" placeHolder="' . _("Password") . '">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" name="login" class="btn btn-default">' . _("Login") . '</button>
                        </div>
                    </div>
                  </form>';
               } else {

                    // TODO: Register the user, create new User() and verify i.e. give feedback to the user about it
                    if (isset($_POST['agreeManifesto']) && isset($_POST['agreeLicense'])) {
                        
                        echo '<p>' . _('Your registration was successfull.') . '</p>';
                        echo '<p>' . _('Thanks for your help to this project!.') . '</p>';
                    } else {
                        echo "You moron.";
                    }
               }
    echo '    </div>
          </div>';
          
//     echo '<div class="row">
//               <div class="col-sm-12">
//                   <h3>' . _("Code of Conduct - This is the way we want to work together:") . '</h3>
//                   <ul>
//                     <li>' . _('We always assume good intention in the communication and interaction with others.') . '</li>
//                     <li>' . _('We include everybody who wants to work with us.') . '</li>
//               </div>
//           </div>';

    
    include("templates/footer.php.inc");
?>
