<?php
    /**
    * @copyright Copyright 2018 Mario Fux (mario@project-flauna.org)
    * @license https://www.gnu.org/licenses/gpl.txt GNU GPL
    *
    * This file is part of Project Flauna.
    * 
    * Projekt Flauna is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
    *
    * Project Flauna is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with Project Flauna. If not, see <http://www.gnu.org/licenses/>.
    */
    
    include_once("templates/header.php.inc");
    include_once("templates/navigation.php.inc");
    include("views/PaginationView.php");
?>
        <div class="container">
        <div class="row">
<!--             <div class="col-sm-4"><h1><a href="display-test.php">Project Blumi</a></h1></div> -->
            <div class="col-sm-4">
                <form class="form-inline" action="" method="POST">
                    <div class="form-group">
                        <select name="db">
                            <option value="birds" <?php if($table == "birds") { echo "selected"; } echo ">" . _('Birds of Europe'); ?> </option>
                            <option value="insects" <?php if($table == "insects") { echo "selected"; } echo ">" . _('Insects of Earth'); ?></option>
                            <option value="trees" <?php if($table == "trees") { echo "selected"; } echo ">" . _('Trees of Earth'); ?></option>
                            <option value="flora" <?php if($table == "flora") { echo "selected"; } echo ">" . _('Flora of Europe'); ?></option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-default"><?php echo _("Change table") ?></button>
                </form>
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
<!--         </div> -->
        <?php
         
            // Set some variables for data display
            $entriesPerPage = 9;
            $currentPageRange = 0;
            $paginationSize = 5;

//             echo "<div class=\"row\">";
            // Check if there are multiple pages to be shown
            if (isset($_GET['page'])) {
                $currentPage = $_GET['page'];
//                 echo "<div class=\"col-sm-4\">Page: $currentPage ";
                $currentPageRange = ($currentPage - 1) * $entriesPerPage;
                $_SESSION['currentPage'] = $currentPage;
            } else {
//                 echo "<div class=\"col-sm-4\">" . _('Start page') . " ";
                unset($_SESSION['currentPage']);
            }
           
            // Check how many entries there are depending of searched name or all of them
            // DOCU: https://phpdelusions.net/pdo_examples/count
            if (isset($_GET['name'])) {
                $name = $_GET['name'];
                $statement = $pdo->prepare("SELECT COUNT(*) FROM $table WHERE label LIKE ?");
                $statement->execute(array("%$name%"));
                $noOfEntries = $statement->fetchColumn();
                echo _('Search query') . ": <i>$name</i></div>";
                $_SESSION['currentQuery'] = $name;
            } else if (isset($_GET['genus'])) {
                $name = $_GET['genus'];
                $statement = $pdo->prepare("SELECT COUNT(*) FROM $table WHERE genera LIKE ?");
                $statement->execute(array("%$name%"));
                $noOfEntries = $statement->fetchColumn();
                echo "- Search term: <i>$name</i></div>";
                $_SESSION['currentQuery'] = $name;            
            } else {
                $sql = "SELECT COUNT(*) FROM $table";
                $noOfEntries = $pdo->query($sql)->fetchColumn();
                echo "</div>";
                unset($_SESSION['currentQuery']);
            }
            
            // Calculate the number of pages to be shown
            $noOfPages = ceil($noOfEntries / $entriesPerPage);
            
            
            // Form for search of certain entries (by label)
//             echo "<div class=\"col-sm-4\">";
//             echo "# of entries: " . $noOfEntries . " - # of pages: " . $noOfPages . " - entries per Page: " . $entriesPerPage;
//             echo "</div>";
//             echo "<div class=\"col-sm-4\">";
//             echo "<form class=\"form-inline\" method=\"GET\" action=\"\">
//                     <div class=\"input-group\">
//                         <input class=\"form-control\" id=\"query\" name=\"name\" type=\"text\" placeHolder=\"" . _("Search query") . "\"/>
//                         <div class=\"input-group-btn\">
//                             <button class=\"btn btn-default\" type=\"submit\">
//                               <i class=\"glyphicon glyphicon-search\"></i>
//                             </button>
//                         </div>
//                     </div>
//                   </form>";
//             echo "</div>";
            echo "</div>";
            
            // Create the pagination depending on the search or not
            // TODO: Make function out of this:
            //       Parameters needed: $noOfPages (not really, as just the hidden feature is depending on this)
            //                          $currentPage,
            //                          $paginationSize, $name (must be set and thus a sign of bad design ;-)
            if ($noOfPages > 1) {
                echo "<div class=\"row\" >";
                echo "<div class=\"col-sm-12\" align=\"center\">";
                if (!isset($currentPage)) {
                    $currentPage = 1;
                }
                
                $pv = new PaginationView();
                if (isset($name)) {
                    $pv->display($currentPage, $noOfPages ,$paginationSize, $name);
                } else {
                    $pv->display($currentPage, $noOfPages ,$paginationSize);
                }

                echo "</div></div>";
            }
            
            // Get the data entries depending of search or not
            if (isset($name)) {   
                $statement = $pdo->prepare("SELECT * FROM $table WHERE label LIKE :name LIMIT :pageRange, :entriesPerPage");
                $statement->execute(array('name' => "%$name%", 'pageRange' => $currentPageRange, 'entriesPerPage' => $entriesPerPage)); 
            } else {
                $statement = $pdo->prepare("SELECT * FROM $table LIMIT :pageRange, :entriesPerPage");
                $statement->execute(array('pageRange' => $currentPageRange, 'entriesPerPage' => $entriesPerPage)); 
            }

            // Fetch the rows/data entries and show them
            if ($noOfEntries > 0) {
                $entryNo = 1;
                $entriesPerRow = 3;
                while ($entry = $statement->fetch()) {
                    $urlArray = explode(DIRECTORY_SEPARATOR, $entry['pics']);
                    $picFileName = end($urlArray);
                    $picFileName = "pics" . DIRECTORY_SEPARATOR . $table . DIRECTORY_SEPARATOR . $picFileName;
                    $templatePic = "white-template.png";
                    // Use line below to get pictures directly from wikimedia
                    // $picFileName = $entry['pics'];
                
                    // Open a row
                    if (($entryNo-1)%3 == 0) {
                        echo "<div class=\"row\">";
                    }
                    
                    // Show title and picture
                    echo "<div class=\"col-sm-4\">";
                    echo "<h3><a href=\"singleentry.php?fid=" . $entry['id'] . "\">" . $entry['label'];
                    if($entry['species'] != "") {
                        echo " - " . $entry['species'];
                    }
                    echo "</a></h3>";
                    if(file_exists($picFileName)) {
                        echo "<a href=\"" . $picFileName . "\"><img title=\"" . _("Click to see in full size") . "\" class=\"img-thumbnail\" src=\"" . str_replace(".jpg", "-squared.jpg",$picFileName);
                    } else {
                        echo "<a href=\"" . $templatePic . "\"><img title=\"" . _("Click to see in full size") . "\" class=\"img-thumbnail\" src=\"" . $templatePic;
                    }
                    echo "\" alt=\"" . $entry['label'] . "\"></a>";
//                     echo "<a href=\"" . $picFileName . "\"><img title=\"Click to see in full size\" class=\"img-thumbnail\" src=\"" . str_replace(".jpg", "-squared.jpg",$picFileName) . "\" alt=\"" . $entry['label'] . "\"></a>";
                    echo "</div>";
                    
                    // Close a row
                    if (($entryNo)%3 == 0) {
                        echo "</div>";
                    }
                    $entryNo++;    
                }
                if (($entryNo-1)%3 != 0) {
                    echo "</div>";
                }
            } elseif (isset($name)) {
                echo "<div class=\"row\">";
                echo "<div class=\"col-sm-4\"></div>";
                echo "<div class=\"col-sm-4\">" . _("No entries found for <i>$name</i>") . "</div>";
                echo "<div class=\"col-sm-4\"></div>";
                echo "</div>";
            } else {
                die("Some error occurred with the database select query.");
            }
            
            // Create the pagination depending on the search or not
            if ($noOfPages > 1) {
                echo "<ul class=\"pagination\">";
                for ($i = 1; $i <= $noOfPages; $i++) {
                    if (isset($currentPage) && $i == $currentPage) {
                        $activeAttribute="class=\"active\"";
                    } elseif (!isset($currentPage) && $i == 1) {
                        $activeAttribute="class=\"active\"";
                    } else {
                        $activeAttribute="";
                    }
                
                    if (isset($name)) {
                        echo "<li $activeAttribute><a href=\"display-test.php?page=$i&name=$name\">$i</a></li>";
                    } else {
                        echo "<li $activeAttribute><a href=\"display-test.php?page=$i\">$i</a></li>";
                    }
                    $activeAttribute="";
                }
                echo "</ul>";
            }
        ?>
        </div>
<?php
    include_once("templates/footer.php.inc");
?>


