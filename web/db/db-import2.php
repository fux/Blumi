<?php
    /**
    * @copyright Copyright 2018 Mario Fux (mario@project-flauna.org)
    * @license https://www.gnu.org/licenses/gpl.txt GNU GPL
    *
    * This file is part of Project Flauna.
    * 
    * Projekt Flauna is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
    *
    * Project Flauna is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with Project Flauna. If not, see <http://www.gnu.org/licenses/>.
    */
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Import CSV to DB</title>
    </head>
    <body>
        <?php
            include_once("db_connection.php.inc");
            
            $dataName = "Test";
            $dataVersion = "01";
            $dbName = "Test";
            $orgUserId = 23;
            $speciesColumn = 0;
            $speciesTable = "tbl_spe";
            
            // TODO: Change this as we might use one db for the moment with several tables
            $pdo->exec("USE flauna");
//             $pdo->exec("CREATE DATABASE $dbName");
//             $pdo->exec("USE $dbName");
            
            $handle = @fopen("/home/fmario/Nextcloud-Nivel/Blumi/Data/$dataName/" . $dbName . "_$dataVersion.csv", "r");
            if ($handle) {
                $lineno=1;
                while (($buffer = fgetcsv($handle)) !== false) {
                    $numberOfColumns = count($buffer);

                    // Handle first line (headers)
                    if ($lineno == 1) {
                        $headers = $buffer;
                        
                        // Create db tables
                        // TODO: label, comment, caption with _de suffix
                        for ($i = 0; $i < $numberOfColumns; $i++) {
                            if($headers[$i] == "species") {
                                $speciesColumn = $i;
                            }
                        
                            $sql = "CREATE TABLE IF NOT EXISTS `tbl_$headers[$i]` (
                                `id` int(11) NOT NULL,
                                `fid` int(11) NOT NULL,
                                `value` varchar(255) NOT NULL,
                                `orgUserId` int(11) NOT NULL,
                                `orgDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                                `r1UserId` int(11) NOT NULL,
                                `r1Date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                                `r2UserId` int(11) NOT NULL,
                                `r2Date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                                `timeCategory` int(11) NOT NULL,
                                `lastUpdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";
                            $pdo->exec($sql);

                            $sql = "ALTER TABLE `tbl_$headers[$i]` ADD PRIMARY KEY (`id`);";
                            $pdo->exec($sql);

                            $sql = "ALTER TABLE `tbl_$headers[$i]` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
                            $pdo->exec($sql);
                        }
                        $lineno++;
                        continue;
                    }
                    
                    // DEBUG output
                    echo "<p> $numberOfColumns Felder in Zeile $lineno: <br /></p>\n";

                    // TODO: Generate the complete (unabbreviated) species name (get it from genera, compare with first (before .) of species 
                    // Check if $species is empty!
                    $species = $buffer[$speciesColumn];
                    
                    if(empty($species)) {
                        echo "<p>On line # $lineno there is a species with an empty species column.</p>";
                        $lineno++;
                        continue;
                    }
                    
                    $statement = $pdo->prepare("SELECT fid FROM $speciesTable WHERE species = ?");
                    $statement->execute(array($species));
                                        
                    if($statement->rowCount() > 0) {
                        echo "<p>Species <i>$species</i> on line #$lineno exists already in database.</p>";
                    } else {
                        // Insert data
                        $statement = $pdo->prepare("INSERT INTO $speciesTable (species) VALUES(?)");
                        $statement->execute(array("$species"));
                        $fid = $pdo->lastInsertId();
                        
                        // Add data to tables for this new species
                        for ($i = 0; $i < $numberOfColumns; $i++) {
                            echo "<b>" . $headers[$i] . ":</b> " . $buffer[$i] . "<br />\n";
                            $statement = $pdo->prepare("INSERT INTO tbl_" . $headers[$i] . " (fid, value, orgUserId) VALUES (:fid, :value, :orgUserId)");
                            $statement->execute(array('fid' => $fid, 'value' => $buffer[$i], 'orgUserId' => $orgUserId));
                        }
                    }
                                    
                    $lineno++;
    
                    
//                     $statement = $pdo->prepare("INSERT INTO Alpenflora
//                             (r, label, abstract, bild, bildbeschreibung, taxonName, taxonWissname, wikiPage, farbe, groesseCM, blueteZeit)
//                             VALUES (:r, :label, :abstract, :bild, :bildbschreibung, :taxonName, :taxonWissname, :wikiPage, :farbe, :groesseCM, :blueteZeit)");
//                     $statement->execute(array('r' => $lineArray[0], 'label' => $lineArray[1], 'abstract' => $lineArray[2], 'bild' => $lineArray[3],
//                             'bildbschreibung' => $lineArray[4], 'taxonName' => $lineArray[6], 'taxonWissname' => $lineArray[7],
//                             'wikiPage' => $lineArray[24], 'farbe' => $lineArray[25], 'groesseCM' => $lineArray[26], 'blueteZeit' => $lineArray[27]));
                }
                if (!feof($handle)) {
                    echo "Fehler: unerwarteter fgets() Fehlschlag\n";
                }
                fclose($handle);
            }
            
        ?>
    </body>
</html>


