<?php
    /**
    * @copyright Copyright 2018 Mario Fux (mario@project-flauna.org)
    * @license https://www.gnu.org/licenses/gpl.txt GNU GPL
    *
    * This file is part of Project Flauna.
    * 
    * Projekt Flauna is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
    *
    * Project Flauna is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with Project Flauna. If not, see <http://www.gnu.org/licenses/>.
    */

    include("templates/header.php.inc");
    include("templates/navigation.php.inc");
    
    echo '<div class="container">
          <div class="row">
              <div class="col-sm-12"><h2>' . _("Verification") . '</h2></div>
          </div>';
    echo '<div class="row">
              <div class="col-sm-12">';
                // TODO: This code should be moved to navigation.php.inc so that menu is uptodate if verification is successful and thus user logged in
                if(isset($_GET['c'])) {
                    // Check if there is a verification code like $verificationCode in the table
                    // If yes, set the account as verified = 1 and login as user with this userid
                    // If no, give some error message and call them a bad robot

                    $verificationCode = $_GET['c'];
                    
                    // DEBUG output
//                     echo "<p>Bla: $verificationCode</p>";
                    
                    // Remove last two letters from verficiationCode
                    $verificationCode = substr($verificationCode, 0, -2);
                    
//                     echo "<p>Bla: $verificationCode</p>";
                    
                    $statement = $pdo->prepare("SELECT userid FROM verification WHERE verificationCode = ?");
                    $statement->execute(array($verificationCode));
                    
                    if($statement->rowCount() == 1) {
                        
                        $user= $statement->fetch();
                        $userid = $user['userid'];
                        
                        $_SESSION['userid'] = $userid;
                        
                        $statement = $pdo->prepare("UPDATE users SET verified = 1 WHERE id = :userid");
                        $statement->execute(array('userid' => $userid));
                        
                        echo "<p>" . _("Your account was successfully verified.") . "</p>";
                        echo "<p>" . _("Please enjoy our open and free data and thanks a lot for your future contributions.") . "</p>";
                    } 
                } else {
                    echo "<p>" . _("Your visit of this webpage misses a verification code.") . "</p>";
                    echo "<p>" . _("Have fun going back to <a href='index.php'>home</a> and enjoy our open and free data.") . "</p>";
                    echo "<p>" . _("For the case you want to contribute please <a href='registration.php'>register</a> and come back with a verification code ;-)") . "</p>";
                }
    echo '    </div>
          </div>';
          

    include("templates/footer.php.inc");
?>
