<?php
    /**
    * @copyright Copyright 2018 Mario Fux (mario@project-flauna.org)
    * @license https://www.gnu.org/licenses/gpl.txt GNU GPL
    *
    * This file is part of Project Flauna.
    * 
    * Projekt Flauna is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
    *
    * Project Flauna is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with Project Flauna. If not, see <http://www.gnu.org/licenses/>.
    */

    include("templates/header.php.inc");
    include("templates/navigation.php.inc");
    echo '<div class="container">
          <div class="row">
              <div class="col-sm-12"><h2>' . _("Get Involved") . '</h2></div>
          </div>';
          
    echo '<div class="row">
              <div class="col-sm-12">
                  <h3>' . _("How to contact us:") . '</h3>
                  <ul>
                    <li>' . _('Write to our <a href="http://lists.project-flauna.org/mailman/listinfo/discuss-de">mailing list</a> and discuss with us.') . '</li>
                    <li>' . _('Write us an <a href="mailto:contact@project-flauna.org">email</a>.') . '</li>
                    <li>' . _('Chat with us on IRC on <a href="https://webchat.freenode.net/">Freenode.net</a> in the channel #flauna') . '</li>
                    <li>' . _('Meet us at our of our live events.') . '</li>
                  </ul>
              </div>
          </div>';
          
    echo '<div class="row">
              <div class="col-sm-12">
                  <h3>' . _("People that could help us (in alphabetical order):") . '</h3>
                  <ul>
                    <li><b>' . _("Artists") . '</b>' . _("- e.g. for the look and feel of the webpage and its icons and graphics") . '</li>
                    <li><b>' . _("Biologists") . '</b>' . _(" - e.g. for the accuracy of the data and its systematics") . '</li>
                    <li><b>' . _("Contributors in general") . '</b>' . (" - e.g. to enhance our data and tell people of this project") . '</li>
                    <li><b>' . _("Database specialists") . '</b>' . _(" - e.g. to our database schemas or access.") . '</li>
                    <li><b>' . _("Mappers") . '</b>' . _(" - e.g. to map our fauna and flora and add some geographical data.") . '</li>
                    <li><b>' . _("Programmers") . '</b>' . _(" - e.g. to develop new features for this website and applications") . '</li>
                    <li><b>' . _("Translators") . '</b>' . _(" - e.g. to translate the data and this webpage") . '</li>
                  </ul>
                  <p>' . _("There are only two things you should agree on:") . '</p>
                  <ol>
                    <li>' . _('You agree to license your contributed data under the terms of the <a href="https://creativecommons.org/licenses/by-nd/4.0/deed.de">CC BY SA license</a>.') . '</li>
                    <li>' . _('You agree to the <a href="manifesto.php">manifesto</a> and our <a href="manifesto.php">Code of Conduct</a>.') . '</li>
                  </ol>
              </div>
          </div>';
          
    echo '<div class="row">
              <div class="col-sm-12">
                  <h3>' . _("Some further ideas how and where you could contribute (in no special order):") . '</h3>
                  <ul>
                    <li>' . _('Create lessons for pupils and educators to work with and enhance Project Flauna.') . '</li>
                    <li>' . _('An AR or VR app.') . '</li>
                    <li>' . _('Extract more data vom DBpedia, Wikipedia or somewhere else.') . '</li>
                    <li>' . _('New categories of data') . '</li>
                    <li>' . _('Work on a satellite to get us free and open satellite images.') . '</li>
                    <li>' . _('One of your genious ideas. <a href="mailto:discuss-de@project-flauna.org">Talk with us</a> about it!') . '</li>
                  </ul>
              </div>
          </div>';
          
    include("templates/footer.php.inc");
?>
